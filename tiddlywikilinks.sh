#!/bin/bash
while (true);do read url;
  if [ -z "$url" ] ;then 
    echo -en "" # "\rERROR:NO URL GIVEN\r";
  else
    domain=$(echo "$url"|sed 's/.\+\/\///g;s/\/.\+//g' )
    mydate=""
    dates=$(wget -O - -o /dev/null --save-headers "$url" |grep -e ^Date: -e 'meta name="date"' -e pubdate -e '<meta property="article:published_time"' -e '<time itemprop="dateCreated" datetime=' ); 
## FIRST check og meta pubdate title    
    meta_pubdate=$(echo "$dates"|sed 's/<meta property="article:published_time"/\n<meta property="article:published_time"/g;s/<time itemprop="dateCreated" datetime=/\n<time itemprop="dateCreated" datetime=/g' |grep -e '<time itemprop="dateCreated" datetime=' -e '<meta property="article:published_time"' |head -n1 |cut -d\" -f4 )
    [[ ! -z "$meta_pubdate" ]] &&  mydate=$(date -u -d "$meta_pubdate" +%Y-%m-%d 2>/dev/null) 
    ##DEBUG echo META:$meta_pubdate 
    if [ -z "$mydate" ] ; then ## IF B  
        # Full text pubdate from some cms
        body_pubdate=$(echo "$dates"|sed 's/.\+pubdate/\npubdate/g;s/pubdate"/pubdate/g'|grep ^pubdate|head -n1 |cut -d\" -f2)
        [[ ! -z "$body_pubdate" ]] &&  mydate=$(date -u -d "$body_pubdate" +%Y-%m-%d 2>/dev/null) 
        ##DEBUG echo BODY:$body_pubdate
        if [ -z "$mydate" ] ; then ## IF C
            metadate=$(echo "$dates"|sed 's/<meta name="date"/\n<meta name="date"/g'|grep 'meta name="date'|cut -d\" -f4);
            [[ ! -z "$metadate" ]] &&  mydate=$(date -u -d "$metadate" +%Y-%m-%d 2>/dev/null) 
            if [ -z "$mydate" ] ;then ## IF D
                headerdate=$(echo "$dates"|grep ^Date:|sed 's/Date:\(\|\)//g');
                [[ ! -z "$headerdate" ]] &&  mydate=$(date -u -d "$headerdate" +%Y-%m-%d 2>/dev/null) 
            fi #ENDIF D
        fi #ENDIF C
      fi ##ENDIF B
    ### show table in default mode
    if [[ "$MODE" !=  "linkonly" ]];then  echo -n "| ";fi 
    echo -n "[[ $mydate @ $domain → "$(curl -Lks "$url"|  perl -l -0777 -ne 'print $1 if /<title.*?>\s*(.*?)\s*<\/title/si'|sed 's/|//g')" |$url ]] ";
    if [[ "$MODE" !=  "linkonly" ]];then  echo -n " |";fi 
    echo 
  fi
done 
  
